package core.general;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

public class ExcelReader {
    public static void rowsCount(String fileName) throws IOException, InvalidFormatException {
        // Creating a Workbook from an Excel file
        Workbook workbook = WorkbookFactory.create(new File("./Sheets/" + fileName + ".xlsx"));

        // Getting the Sheet at index zero
        Sheet sheet = workbook.getSheetAt(0);

        envGlobals.rowsCount = sheet.getPhysicalNumberOfRows();
    }

    public static void read(String fileName) throws IOException, InvalidFormatException {
        // Creating a Workbook from an Excel file (.xls or .xlsx)
        Workbook workbook = WorkbookFactory.create(new File("./Sheets/" + fileName + ".xlsx"));

        // Getting the Sheet at index zero
        Sheet sheet = workbook.getSheetAt(0);

        // Create a DataFormatter to format and get each cell's value as String
        DataFormatter dataFormatter = new DataFormatter();

        // Using a for-each loop to iterate over the rows and columns
        for (Row row: sheet) {
            for(Cell cell: row) {
                String cellValue = dataFormatter.formatCellValue(cell);
                if (fileName.contains("1"))
                    envGlobals.requestList1.add(cellValue);
                else
                    envGlobals.requestList2.add(cellValue);
            }
        }

        // Closing the workbook
        workbook.close();
    }

}
