package core.general;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import core.configuration.TestsConfig;
import io.restassured.RestAssured;
import io.restassured.filter.log.LogDetail;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import java.awt.*;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.sql.SQLException;
import java.util.Date;
import java.util.Properties;

public abstract class BaseTest {
    private static ExtentTest logger;
    private static String isEnableReporting = TestsConfig.getConfig().getIsEnableReporting();
    private static Date time = ReusableMethods.getTime();

    @BeforeSuite
    public void startReport() throws IOException, InvalidFormatException {
        if (isEnableReporting.equals("true")) {
            MainCall.startReport();
        }

        ExcelReader.read(envGlobals.file1);
        ExcelReader.read(envGlobals.file2);
        ExcelReader.rowsCount(envGlobals.file1);
    }

    @BeforeMethod
    public void setup(Method method) throws MalformedURLException, AWTException, SQLException {
        if (isEnableReporting.equals("true")) {
            logger = MainCall.getExtentReport().startTest(method.getName(), "");
            logger.setStartedTime(ReusableMethods.getTime());
        }

        try {
            Properties props = new Properties();

            props.load(getClass().getClassLoader().getResourceAsStream("config.properties"));

            //Rest Assured config
            RestAssured.enableLoggingOfRequestAndResponseIfValidationFails(LogDetail.ALL);
            RestAssured.useRelaxedHTTPSValidation();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @AfterMethod
    public void tearDown(ITestResult result) {
        if (isEnableReporting.equals("true")) {
            if (result.getStatus() == ITestResult.FAILURE) {
                logger.log(LogStatus.FAIL, "Test Case Failed reason is: " + result.getThrowable());
//                logger.log(LogStatus.FAIL, logger.addScreenCapture(Screenshots.takeScreenshot(result.getMethod().getMethodName())));
            } else if (result.getStatus() == ITestResult.SKIP) {
                logger.log(LogStatus.SKIP, "Test Case Skipped is: " + result.getName());
            } else {
                logger.log(LogStatus.PASS, result.getMethod().getMethodName() + " is Passed");
            }

            logger.setEndedTime(time);
            MainCall.getExtentReport().endTest(logger);
        }
    }

    @AfterSuite
    public void endReport() {
        if (isEnableReporting.equals("true")) {
            MainCall.getExtentReport().flush();
            MainCall.getExtentReport().close();
        }
    }
}
