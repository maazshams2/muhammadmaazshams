package core.general;

import io.restassured.http.ContentType;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.Calendar;
import java.util.Date;

import static io.restassured.RestAssured.given;

public class ReusableMethods {
    public ReusableMethods(){
    }

    public static String generateAutoSubject() {
        String s = RandomStringUtils.randomNumeric(5);
        String autoSub = "Auto-Test-" + s ;

        return autoSub;
    }

    public static Date getTime() {
        Calendar calendar = Calendar.getInstance();
        return calendar.getTime();
    }

}
