package core.general;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class envGlobals {
    // Rest Assured Global variables
    public static RequestSpecification request = null;
    public static Response response1 = null;
    public static Response response2 = null;

    // Files
    public static String file1 = "File1";
    public static String file2 = "File2";

    // List of urls
    public static int rowsCount = 0;
    public static List<String> requestList1 = new ArrayList<>();
    public static List<String> requestList2 = new ArrayList<>();

    // List of urls
    public static JSONObject obj1 = new JSONObject();
    public static JSONObject obj2 = new JSONObject();


}
