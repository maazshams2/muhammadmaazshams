package core.general;

import com.relevantcodes.extentreports.ExtentReports;
import core.configuration.TestsConfig;

public class MainCall {
    public MainCall(){}
    public static ExtentReports extent;


    public static ExtentReports startReport() {
        extent = new ExtentReports(System.getProperty("user.dir") + "/build/reports/ExtentReport.html", true);

        extent.addSystemInfo("Environment", TestsConfig.getConfig().getEnvironment());

        return extent;
    }

    public static ExtentReports getExtentReport() {
        if (extent != null) {
            return extent;
        } else {
            throw new IllegalStateException("Extent Report object not initialized");
        }
    }

}
