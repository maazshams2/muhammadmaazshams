package core.configuration;

import core.configuration.testProperties.PropertiesLoader;
import core.configuration.testProperties.Property;
import core.configuration.testProperties.PropertyFile;

@PropertyFile("config.properties")
public class TestsConfig {

    private static TestsConfig config;

    public static TestsConfig getConfig() {
        if (config == null) {
            config = new TestsConfig();
        }
        return config;
    }

    public TestsConfig() {
        PropertiesLoader.populate(this);
    }

    @Property("Device")
    private String Device ="";
    @Property("cycleName")
    private String cycleName = "";

    @Property("releaseVersion")
    private String releaseVersion = "";

    @Property("projectKey")
    private String projectKey = "";

    @Property("env")
    private String env = "";

    @Property("baseUri")
    private String baseUri = "";

    @Property("dbUrl")
    private String dbUrl = "";

    @Property("dbUsername")
    private String dbUsername = "";

    @Property("dbPassword")
    private String dbPassword = "";

    @Property("project")
    private String project = "";

    @Property("dbConfig")
    private String dbConfig = "";

    @Property("loginUserName")
    private String loginUserName = "";

    @Property("loginPassword")
    private String loginPassword = "";

    @Property("loginUrl")
    private String loginUrl = "";

    @Property("dbConfigMySql")
    private String dbConfigMySql = "";

    @Property("dbUserNameMySql")
    private String dbUserNameMySql = "";

    @Property("dbPasswordMySql")
    private String dbPasswordMySql = "";

    @Property("dbServerMySql")
    private String dbServerMySql = "";

    @Property("environment")
    private String environment = "";

    @Property("isEnableReporting")
    private String isEnableReporting = "";


    public String getCycleName() {
        return cycleName;
    }

    public String getReleaseVersion() {
        return releaseVersion;
    }

    public String getProjectKey() {
        return projectKey;
    }

    public String getDbUrl() {
        return dbUrl;
    }

    public String getBaseUri() {
        return baseUri;
    }

    public String getDbUsername() { return dbUsername; }

    public String getDbPassword() { return dbPassword; }

    public String getProject() { return project; }

    public String getDbConfig() {
        return dbConfig;
    }

    public String getDevice() {return Device;}

    public String getLoginUserName() {return loginUserName;}

    public String getLoginPassword() {return loginPassword;}

    public String getLoginUrl() {return loginUrl;}

    public String getDbConfigMySql() {return dbConfigMySql;}

    public String getDbUserNameMySql() {return dbUserNameMySql;}

    public String getDbPasswordMySql() {return dbPasswordMySql;}

    public String getDbServerMySql() {return dbServerMySql;}

    public String getEnvironment() {return environment;}

    public String getIsEnableReporting() {return isEnableReporting;}
}
