package step_definitions.itinerary;

import core.general.envGlobals;
import io.restassured.response.Response;
import requestPayloads.BackEndPayloads;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import static core.general.envGlobals.request;
import static io.restassured.RestAssured.given;
import static org.apache.commons.lang3.StringUtils.isNumeric;
import static org.hamcrest.core.IsEqual.equalTo;

public class Class1 {
    public Class1(){
    }

    private static Response response;
    private static boolean bool;

//******************************************            Fixed Functions            ******************************************

    public static void givenFunction(String[] headers, String[] path, String[] query, String function) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
//        List<Map<String, String>> list = dt.asMaps(String.class, String.class);
        Map<String, String> headersMap = new HashMap<>();
        Map<String, String> pathMap = new HashMap<>();
        Map<String, String> queryMap = new HashMap<>();

//        HEADERS
//        String headersArray[] = headers.get(0).get("Headers").split(",");
        if(headers.length>1){
            for (int i=0 ; i<headers.length ; i=i+2){
                headersMap.put(headers[i], headers[i + 1]);
            }
        }
//        PATH PARAMS
//        String pathParamArray[] = list.get(0).get("Path Params").split(",");
        if(path.length>1) {
            for (int i = 0; i < path.length; i = i + 2) {
                pathMap.put(path[i], path[i + 1]);
            }
        }
//        QUERY PARAMS
//        String queryParamArray[] = list.get(0).get("Query Params").split(",");
        if(query.length>1) {
            for (int i = 0; i < query.length; i = i + 2) {
                queryMap.put(query[i], query[i + 1]);
            }
        }

        envGlobals.request =
                given()
//                        .log().all()
                        .headers(headersMap)
                        .pathParams(pathMap)
                        .queryParams(queryMap);

//        REQUEST PAYLOAD
        if (function!= null && !function.isEmpty()) {
            Method method = null;

            method = BackEndPayloads.class.getMethod(function);
            BackEndPayloads backEndPayloadsInstance = new BackEndPayloads();

            request.body(method.invoke(backEndPayloadsInstance));
        }
    }

    public static Response whenFunction(String method, String endPoint){
        switch (method){
            case "get":
                response = request
                        .when()
                            .get(endPoint);
                break;
            case "post":
                response = request
                        .when()
                            .post(endPoint);
                break;
            case "put":
                response = request
                        .when()
                            .put(endPoint);
                break;
            case "delete":
                response = request
                        .when()
                            .delete(endPoint);
                break;
            case "patch":
                response = request
                        .when()
                            .patch(endPoint);
                break;
        }

        return response;
    }

    public static void thenFunction(Response res, int code){
        res
            .then()
//                .log().all()
                .assertThat().statusCode(code);
    }

    public static void andAssertionFunction(String[] path, String[] value) throws InterruptedException {

        for (int i=0 ; i<path.length ; i++) {
//            For INTEGER data type
            if (isNumeric(value[i])) {
                response
                    .then()
                        .body(path[i], equalTo(Integer.parseInt(value[i])));
            }
//            For BOOLEAN data type set as TRUE
            else if (value[i].equalsIgnoreCase("true")){
                bool = true;
                response
                    .then()
                        .body(path[i], equalTo(bool));
            }
//            For BOOLEAN data type set as FALSE
            else if(value[i].equalsIgnoreCase("false")) {
                bool = false;
                response
                    .then()
                        .body(path[i], equalTo(bool));
            }
//            For NULL
            else if(value[i] == null || value[i] == "" || value[i].isEmpty()) {
                response
                    .then()
                        .body(path[i], equalTo(null));
            }
//            Final will remain for STRING data type
            else{
                value[i] = value[i].substring(1,value[i].length()-1);
                response
                    .then()
                        .body(path[i], equalTo(value[i]));
            }
        }
    }
//***************************************************************************************************************************

}
