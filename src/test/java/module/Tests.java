package module;

import core.general.envGlobals;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.annotations.Test;
import step_definitions.itinerary.Class1;
import core.general.BaseTest;

import java.lang.reflect.InvocationTargetException;

public class Tests extends BaseTest{
    public Tests(){
    }

    @Test
    public void compare() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, JSONException {
        String[] headersArray = {"Content-Type", "application/json"};
        String[] pathArray = {""};
        String[] queryArray = {""};
        String functionName = "";

        Class1.givenFunction(headersArray, pathArray, queryArray, functionName);

        for (int i=0 ; i< envGlobals.rowsCount ; i++) {
            // File1 request
            envGlobals.response1 = Class1.whenFunction("get", envGlobals.requestList1.get(i));
            Class1.thenFunction(envGlobals.response1, 200);

            // File2 request
            envGlobals.response2 = Class1.whenFunction("get", envGlobals.requestList2.get(i));
            Class1.thenFunction(envGlobals.response2, 200);

            envGlobals.obj1 = new JSONObject(envGlobals.response1.asString());
            envGlobals.obj2 = new JSONObject(envGlobals.response2.asString());
            if (envGlobals.obj2.toString().equals(envGlobals.obj1.toString()))
                System.out.println(envGlobals.requestList1.get(i) + " equals " + envGlobals.requestList2.get(i));
            else
                System.out.println(envGlobals.requestList1.get(i) + " not equals " + envGlobals.requestList2.get(i));
        }

    }

}
