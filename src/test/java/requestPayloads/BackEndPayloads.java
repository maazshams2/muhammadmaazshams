package requestPayloads;

public class BackEndPayloads {
    public BackEndPayloads(){}

    public static String req = "";

    public static String addPreference(){
        req = "{\n" +
                "\t\"timeFormat\": 1,\n" +
                "\t\"profileId\": 2,\n" +
                "\t\"profileValue\": 2,\n" +
                "\t\"id\": null,\n" +
                "\t\"subscribers\": [{\n" +
                "\t\t\"personId\": 71403,\n" +
                "\t\t\"greeting\": \"Laraib\",\n" +
                "\t\t\"key\": \"71403\",\n" +
                "\t\t\"isUpdated\": false,\n" +
                "\t\t\"salutation\": \"\",\n" +
                "\t\t\"typeValue\": 2,\n" +
                "\t\t\"isActive\": true,\n" +
                "\t\t\"itineraryInitial\": true,\n" +
                "\t\t\"itineraryRevised\": false,\n" +
                "\t\t\"itineraryFinal\": false,\n" +
                "\t\t\"emailAddress\": \"\",\n" +
                "\t\t\"emailType\": \"TO\",\n" +
                "\t\t\"typeId\": 1\n" +
                "\t}]\n" +
                "}";
        return req;
    }

    public static String createEmployee(){
        req = "{\n" +
                "    \"name\": \"maaz\",\n" +
                "    \"job\": \"leader\",\n" +
                "    \"salary\": \"10\",\n" +
                "    \"age\": \"25\"\n" +
                "}";

        return req;
    }

    public static String updateEmployee(){
        req = "{\n" +
                "    \"name\": \"morpheusssss\",\n" +
                "    \"job\": \"zion resident\"\n" +
                "}";

        return req;
    }

    public static String loginSuccessfully(){
        req = "{\n" +
                "    \"email\": \"peter@klaven\",\n" +
                "    \"password\": \"cityslicka\"\n" +
                "}";
        return req;
    }



}
